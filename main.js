'use strict'

const two = require('two')
    , five = require('five')

module.exports = _ => two() * five() * two() + two()
