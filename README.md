# twentytwo [![NPM version](https://img.shields.io/npm/v/twentytwo.svg)](https://www.npmjs.com/package/twentytwo)
> A micropackage that returns a utility function to get the number
twentytwo.

## What does it do?
This package allows you to use a handy utility function to easily access
the number `22` (twentytwo)!

## Installation
```console
npm i twentytwo
```

## Usage example
```js
const twentytwo = require('twentytwo')()
console.log(twentytwo)
```

<h2 style="font-size: 5px">Is this a joke?</h2>
<span style="font-size: 5px">Yes. But don't tell anyone.</span>
