'use strict'

const assert = require('chai').assert
    , twentytwo = require('..')

describe('Twentytwo', () => {
	it('should return 22', () => {
		assert.equal(twentytwo(), 22)
	})
})
